'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "roles",
      [
        {
          id: "7b139389-1ba4-400b-9055-81f47526f0cb",
          role: "SDE 1",
          userId: "0e519e24-73f8-49c6-9a07-cdd28c6f0be4",
          createdAt: "2022-02-03T17:19:40.389Z",
          updatedAt: "2022-02-03T18:26:23.055Z",
        },
        {
          id: "ef583713-e45b-4370-ba5c-6563e47e2bc8",
          role: "SDE 2",
          userId: "0e519e24-73f8-49c6-9a07-cdd28c6f0be4",
          createdAt: "2022-02-03T17:19:40.389Z",
          updatedAt: "2022-02-03T18:26:23.055Z",
        },
      ],
      {}
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete("roles", null, {});
  }
};
