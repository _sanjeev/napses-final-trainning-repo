const Models = require("models");

module.exports = class CreateAadharQuery {
  constructor(id, aadharNumber, userId) {
    this.details = {
      id,
      aadharNumber,
      userId
    };
  }
   async get() {
    // return Models.Aadhar.create({
    //   id: this.details.id,
    //   aadharNumber: this.details.aadharNumber,
    // });
    let user = await Models.User.findOne({
        where: {
            id: this.details.userId,
        }
    });
    return user.createAadhar ({
        id: this.details.id,
        aadharNumber: this.details.aadharNumber
    })
  }
};
