const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, whenResult, composeResult } = require("lib");
const Result = require("folktale/result");
const db = require("db/repository");
const uuid = require("uuid");
const CreateAadharQuery = require("resources/users/Aadhar/queries/createAadhar.js");
const CreateAadharValidation = require("./../validations/create-aadhar-validation");

async function post(req, res) {
  const userId = req.params.id;
  const { aadharNumber } = req.body;
  logInfo("Request to create aadhar", {
    aadharNumber,
  });

  const id = uuid.v4();

  // const validationResult = await CreateAadharValidation.validate({
  //   aadharNumber,
  // });

  // const response = await whenResult(() => {
  //   return db.execute(new CreateAadharQuery(id, aadharNumber, userId));
  // })(validationResult);

  const response = await composeResult (
    () => {
      return db.execute (new CreateAadharQuery (id, aadharNumber, userId));
    },
    CreateAadharValidation.validate
  )({aadharNumber});
  
  return respond(
    response,
    "Successfully created aadhar",
    "Failed to create aadhar"
  );
}

Route.withOutSecurity().noAuth().post("/users/:id/aadhar", post).bind();
// module.exports.post = post;
