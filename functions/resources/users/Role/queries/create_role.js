const Models = require("models");

module.exports = class CreateRoleQuery {
  constructor(id, role, userId) {
    this.details = {
      id,
      role,
      userId,
    };
  }
  async get() {
    const user = await Models.User.findOne({
      where: {
        id: this.details.userId,
      },
    });
    // // const users = await Models.User.findAll();
    // const userRoles = this.details.roles.map(async (role) => {
    //   return await user.addRole(role);
    // })

    return user.createRole({
      id : this.details.id,
      role: this.details.role,
    });
  }
};
