const {
  validate,
  notEmpty,
  isEmail,
  minValue,
  hasLengthOf,
  shouldBeUuid,
  isMobileNumber,
} = require("validation");
const findLength = require("../../../../lib/validations/findLength");

const rule = {
  fullName: [
    [notEmpty, "FullName should not be empty!"],
    [
      (value, obj) => {
        return minValue(2, findLength(value));
      },
      "FullName length should always be greater than or equals to 2."
    ],
    
  ],
  countryCode: [
    [notEmpty, "countryCode should not be empty!"],
    [
      (value, obj) => {
        return hasLengthOf(2, JSON.stringify(value));
      },
      "countryCode Must be of length 2",
    ],
  ],
};

module.exports.validate = async (data) => validate(rule, data);
