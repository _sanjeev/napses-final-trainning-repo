const Models = require("models");

module.exports = class UpdateUserQuery {
  constructor(id, fullName, countryCode) {
    this.details = {
      id,
      fullName,
      countryCode,
    };
  }
  async get() {
    const user = await Models.User.update({
      fullName: this.details.fullName,
      countryCode: this.details.countryCode,
    }, {
      where: {
        id: this.details.id
      }
    });

    const users = await Models.User.findOne({
      where : {
        id: this.details.id
      }
    })
    
    return users;
  }
};
