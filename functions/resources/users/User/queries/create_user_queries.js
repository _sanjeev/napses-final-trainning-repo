const Models = require("models");

module.exports = class CreateUserQuery {
  constructor(id, fullName, countryCode) {
    this.details = {
      id,
      fullName,
      countryCode,
    };
  }
  get() {
    return Models.User.create({
      id: this.details.id,
      fullName: this.details.fullName,
      countryCode: this.details.countryCode,
    });
  }
};
