const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond } = require ('lib');
const Result = require ('folktale/result');
const Models = require("models");
const db = require ('db/repository');
const DeleteUserQuery = require ('resources/users/User/queries/delete_user.js');

const deleteUser = async(req, res) => {
    const id = req.params.id;
    const response = await db.execute (new DeleteUserQuery(id)); 
    return respond(response, 'Successfully delete user', 'Failed to delete user');
}

Route.withOutSecurity().noAuth().delete('/users/:id', deleteUser).bind();
module.exports.deleteUser = deleteUser;


