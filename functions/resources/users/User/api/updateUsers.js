const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, whenResult, composeResult } = require("lib");
const Result = require("folktale/result");
const Models = require("models");
const db = require("db/repository");
const UpdateUserQuery = require("resources/users/User/queries/update_user.js");
const UpdateUserValidation = require("./../validation/update-user-validation");

const updateValue = async (req, res) => {
  const id = req.params.id;
  const { fullName, countryCode } = req.body;
  logInfo("Request to update the user", {
    fullName,
    countryCode,
  });

  const response = await composeResult (
    () => {
      const user =  db.execute (new UpdateUserQuery(id, fullName, countryCode));
      return user;
    },
    UpdateUserValidation.validate
  )({fullName, countryCode});

  return respond(response, "Successfully update the user", "Failed to update user");
};

Route.withOutSecurity().noAuth().put("/users/:id", updateValue).bind();

