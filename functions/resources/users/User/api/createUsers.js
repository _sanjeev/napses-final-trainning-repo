const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond, whenResult, composeResult, withArgs } = require ('lib');
const Result = require ('folktale/result');
const db = require ('db/repository');
const uuid = require ('uuid');
const CreateUserQuery = require ('resources/users/User/queries/create_user_queries.js');
const CreateUserValidation = require ('./../validation/create-user-validation');

async function post (req, res) {
    const {fullName, countryCode} = req.body;

    logInfo ('Request to create user', {
        fullName,
        countryCode
    })
    
    const id = uuid.v4();

    // const validationResult = await CreateUserValidation.validate({fullName, countryCode});
    
    // const response = await whenResult(
    //     () => {
    //         return db.execute (new CreateUserQuery (id, fullName, countryCode));
    //     }
    // )(validationResult)

    const response = await composeResult (
        () => db.execute (new CreateUserQuery (id, fullName, countryCode)),        
        CreateUserValidation.validate
    )({fullName, countryCode});

    return respond(response, 'Successfully created user', 'Failed to create user');
}

Route.withOutSecurity().noAuth().post('/users', post).bind();
module.exports.post = post;