const Route = require('route');
const { logInfo } = require('lib/functional/logger');
const { respond, whenResult, composeResult } = require ('lib');
const Result = require ('folktale/result');
const Models = require("models");
const db = require ('db/repository');
const UpdateAddressQuery = require ('./../queries/update_address');
const UpdateAddressValidation = require ('./../validation/update-address-validation');


const updateValue = async (req, res) => {
    const addressId = req.params.addressId;
    // const userId = req.params.userId;
    const {street, city, country} = req.body;

    logInfo ('Request to update the address', {
        street,
        city,
        country
    });

    const validationResult = await UpdateAddressValidation.validate ({street, city, country});

    const response = await whenResult (
        () => {
            return db.execute (new UpdateAddressQuery (addressId, street, city, country));
        }
    )(validationResult);

    // const response = await composeResult (
    //     () => {
    //       const address =  db.execute (new UpdateAddressQuery(id, street, city, country));
    //       return address;
    //     },
    //     UpdateAddressQuery.validate
    //   )({street, city, country});

    return respond(response, 'Successfully update the Address', 'Failed to update Address');
}


Route.withOutSecurity().noAuth().put('/users/:id/addresses/:id', updateValue).bind();