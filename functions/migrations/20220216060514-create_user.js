'use strict';
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable("users",{
        id: {
          type: DataTypes.UUID,
          primaryKey: true
      },
      fullName: {
          type: DataTypes.STRING,
          allowNull: false
      },
      countryCode: {
          type: DataTypes.INTEGER,
          allowNull: false
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
    })
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable("users");
  }
};