'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('addresses', 'userId', {
      type: Sequelize.UUID,
      allowNull: true,
      // references: {model: 'aadhar_cards', key: 'id'}
  }, {})
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('addresses', 'userId')
  }
};
