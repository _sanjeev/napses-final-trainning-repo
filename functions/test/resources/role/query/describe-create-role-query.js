const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const CreateRoleQuery = require ('./../../../../resources/users/Role/queries/create_role');

describe.only("create address query", () => {
    let user,role;
  beforeEach(async () => {
      user = await ds.createSingle(ds.user);
      role = await ds.buildSingle(ds.role, {user});
  });

  it('should create an address', async() => {
      console.log(role);
      const createRoleResponse = await db.execute(new CreateRoleQuery(role.id, role.name, role.user.id));
      verifyResultOk(
          (createRole) => {
              console.log(createRole);
              expect(role.id).eql(createRole.id);
              expect(role.name).eql(createRole.role);
              expect(role.user.id).eql(createRole.userId)
          }
      )(createRoleResponse);

      const fetchRoleResponse = await db.findOne(new RunQuery('select * from public.roles where id=id', {id: role.id}));
      console.log(fetchRoleResponse);
      verifyResultOk(
        (createRole) => {
            expect(role.id).eql(createRole.id);
            expect(role.name).eql(createRole.role);
            expect(role.user.id).eq(createRole.userId);
        }
    )(fetchRoleResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
