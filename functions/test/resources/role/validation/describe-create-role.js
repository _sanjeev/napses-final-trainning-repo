const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateRoleValidation = require("./../../../../resources/users/Role/validation/create-role-validation");

describe("Create Role Validation", () => {
  it("role should not be empty!", async () => {
    let response = await CreateRoleValidation.validate({});
    verifyResultError((error) => {
      expect(error.errorMessage).to.include(
        "role shouldnot be empty"
      );
    })(response);
  });
  
  
  it("When we provide correct result", async () => {
    let response = await CreateRoleValidation.validate({
      role: "Software Developer",
    });
    verifyResultOk(() => {})(response);
  });
});
