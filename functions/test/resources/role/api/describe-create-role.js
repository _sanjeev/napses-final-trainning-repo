const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");

const CreateRoleValidation = require("./../../../../resources/users/Role/validation/create-role-validation");

describe("Should create an roles for an user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let id = uuid.v4();
  beforeEach(() => {
    (req = {
      params: {
        id: id,
      },
      body: {
        role: 'Software Engineer'
      },
    }),
      (res = {
        setHeader: sandbox.spy(),
        send: sandbox.spy(),
        status: sandbox.spy(() => {
          return res;
        }),
      });
  });

  it("should create a role", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk({
            role: 'Software Engineer'
        })
      );
    const response = await TestRoutes.execute(
      "/users/:id/roles",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql({
      status: true,
      message: "Successfully created Role",
      entity: {
        role: 'Software Engineer'
      },
    });
  });

  it("should not create role when validation failed", async () => {
    sandbox
      .mock(CreateRoleValidation)
      .expects("validate")
      .returns(resolveValidationError(["role should be mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/roles",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, ["role should be mandatory"])
    );
  });

  it("something went wrong cannot able to fetch the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/roles",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ApiError(0, "some random error", "Failed to create Role")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
