const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");
const GetAllUser = require("./../../../../resources/users/User/api/getAllUsers");

describe("Get all User", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      body: {},
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  it("should get all user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk([
          {
            fullName: "Sanjeev kr singh",
            countryCode: 65,
          },
        ])
      );
    const response = await TestRoutes.execute("/users", "Get", req, res);
    expect(response).to.eql({
      status: true,
      message: "Successfully fetched all users",
      entity: [
        {
          fullName: "Sanjeev kr singh",
          countryCode: 65,
        },
      ],
    });
  });

  it ('something went wrong cannot able to fetch the user', async () => {
    sandbox.mock(db).expects('execute').returns(resolveError('some random error'));
      const response = await TestRoutes.executeWithError ('/users', 'Get', req, res);
      expect (response).to.eql(new ApiError(0, 'some random error', 'Failed to fetched user'));
  });


  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
