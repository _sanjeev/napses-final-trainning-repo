const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");
const UpdateUserValidation = require("./../../../../resources/users/User/validation/update-user-validation");

describe("Describe Update User", () => {
  let sandbox = sinon.createSandbox();
  let id = uuid.v4();
  let req, res;
  beforeEach(() => {
    req = {
      params: {
        id: id,
      },
      body: {
        fullName: "John Dey",
        countryCode: 65,
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  it("should update an user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk([
          {
            fullName: "John Dey",
            countryCode: 65,
          },
        ])
      );
    const response = await TestRoutes.execute("/users/:id", "Put", req, res);
    expect(response).to.eql({
      status: true,
      message: "Successfully update the user",
      entity: [
        {
          fullName: "John Dey",
          countryCode: 65,
        },
      ],
    });
  });

  it("should not update the user when fullname is not passed", async () => {
    sandbox
      .mock(UpdateUserValidation)
      .expects("validate")
      .returns(resolveValidationError(["FullName is mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ValidationError(0, ["FullName is mandatory"])
    );
  });

  it("should not update when countryCode is not passed", async () => {
    sandbox
      .mock(UpdateUserValidation)
      .expects("validate")
      .returns(resolveValidationError(["countryCode is mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ValidationError(0, ["countryCode is mandatory"])
    );
  });

  it("something went wrong cannot able to update the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random error", "Failed to update user")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
