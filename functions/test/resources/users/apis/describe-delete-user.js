const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");
const deleteUser = require("../../../../resources/users/User/api/deleteUser");

describe("Delete all user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let id = uuid.v4();
  beforeEach(() => {
    req = {
      params: {
        id: id,
      },
      body: {},
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  it("should delete the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk({
          message: "Successfully delete the user",
        })
      );
    const response = await TestRoutes.execute("/users/:id", "Delete", req, res);
    expect(response).to.be.eql({
      status: true,
      message: "Successfully delete user",
      entity: {
        message: "Successfully delete the user",
      },
    });
  });


  it("something went wrong cannot able to delete the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id",
      "Delete",
      req,
      res
    );
    expect(response).to.eql(new ApiError(0, 'some random error', 'Failed to delete user'));
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
