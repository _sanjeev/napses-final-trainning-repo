const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");
const CreateUserValidation = require("./../../../../resources/users/User/validation/create-user-validation");

describe("Describe Create User", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      body: {
        fullName: "Tom Cruse",
        countryCode: 91,
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  it("should create an user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk({
          fullName: "Tom Cruse",
          countryCode: 91,
        })
      );
    const response = await TestRoutes.execute("/users", "Post", req, res);
    expect(response).to.eql({
      status: true,
      message: "Successfully created user",
      entity: {
        fullName: "Tom Cruse",
        countryCode: 91,
      },
    });
  });

  it("should not create user when validation failed", async () => {
    sandbox
      .mock(CreateUserValidation)
      .expects("validate")
      .returns(resolveValidationError("Name of the user is mandatory"));

      const response = await TestRoutes.executeWithError('/users', 'Post', req, res);
      expect(response).to.eql(new ValidationError(0, "Name of the user is mandatory"));
  });

  it("should not create user when validation failed", async () => {
    sandbox
      .mock(CreateUserValidation)
      .expects("validate")
      .returns(resolveValidationError(["countryCode should be mandatory"]));
      const response = await TestRoutes.executeWithError('/users', 'Post', req, res);
      expect(response).to.eql(new ValidationError(0, ["countryCode should be mandatory"]));
  });

  it ('something went wrong cannot able to fetch the user', async () => {
    sandbox.mock(db).expects('execute').returns(resolveError('some random error'));
      const response = await TestRoutes.executeWithError ('/users', 'Post', req, res);
      expect (response).to.eql(new ApiError(0, 'some random error', 'Failed to create user'));
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
