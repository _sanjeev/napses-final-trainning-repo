const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const CreateUserQuery = require ('./../../../../resources/users/User/queries/create_user_queries');

describe("create user query", () => {
    let user
  beforeEach(async () => {
      user = await ds.buildSingle(ds.user);
  });

  it('should create an user', async() => {
      const createdUserResponse = await db.execute(new CreateUserQuery(user.id, user.fullName, user.countryCode));
      verifyResultOk(
          (createdUser) => {
              expect(user.id).eql(createdUser.id);
              expect(user.fullName).eql(createdUser.fullName);
              expect(user.countryCode).eq(createdUser.countryCode);
          }
      )(createdUserResponse);

      const fetchUserResponse = await db.findOne(new RunQuery('select * from public.users where id=id', {id: user.id}));
      verifyResultOk(
        (createdUser) => {
            expect(user.id).eql(createdUser.id);
            expect(user.fullName).eql(createdUser.fullName);
            expect(user.countryCode).eq(createdUser.countryCode);
        }
    )(fetchUserResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
