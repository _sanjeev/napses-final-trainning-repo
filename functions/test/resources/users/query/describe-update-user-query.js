const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const Result = require("folktale/result");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const UpdateUserQuery = require("./../../../../resources/users/User/queries/update_user");

describe("Update user query", () => {
  let user;
  beforeEach(async () => {
    user = await ds.createSingle(ds.user);
  });

  it("should update an user", async () => {
    console.log(user);
    user.fullName = 'Rahul';
    user.countryCode = 65;
    console.log(user);
    const updatedUserResponse = await db.execute(
      new UpdateUserQuery(user.id, user.fullName, user.countryCode)
    );
      verifyResultOk(
          (updatedUser) => {
              expect(user.id).eql(updatedUser.id);
              expect(user.fullName).eql(updatedUser.fullName);
              expect(user.countryCode).eql(updatedUser.countryCode);
          }
      )(updatedUserResponse);

    const fetchUserResponse = await db.findOne(
      new RunQuery("select * from public.users where id=id", { id: user.id })
    );
    console.log('fetchUserResponse', fetchUserResponse);
    verifyResultOk((updatedUser) => {
      expect(user.id).eql(updatedUser.id);
      expect(user.fullName).eql(updatedUser.fullName);
      expect(user.countryCode).eq(updatedUser.countryCode);
    })(fetchUserResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
