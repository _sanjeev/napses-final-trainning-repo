const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateUserValidation = require("../../../../resources/users/User/validation/create-user-validation");

describe("Creates user validation", () => {
  it("FullName Should be mandatory", async () => {
    let response = await CreateUserValidation.validate({});
    verifyResultError((error) => {
      expect(error.errorMessage).to.include("Full Name should not be empty!");
    })(response);
  });

  it ('FullName length is less than 2', async () => {
    let response = await CreateUserValidation.validate ({fullName: 'a'});
    verifyResultError (
      (error) => {
        expect (error.errorMessage).to.include ("fullName length should always greater than 2");
      }
    )(response);
  });

  it("countryCode Should be mandatory", async () => {
    let response = await CreateUserValidation.validate({});
    verifyResultError((error) => {
      expect(error.errorMessage).to.include("countryCode Should not be empty!");
    })(response);
  });

  it ('CountryCode length will be less than 2', async () => {
    let response = await CreateUserValidation.validate({countryCode: 9});
    verifyResultError (
      (error) => {
        expect (error.errorMessage).to.include("countryCode Must be of length 2");
      }
    )(response); 
  });

  it ('CountryCode length will be greater than 2', async () => {
    let response = await CreateUserValidation.validate({countryCode: 911});
    verifyResultError (
      (error) => {
        expect (error.errorMessage).to.include("countryCode Must be of length 2");
      }
    )(response); 
  });

  it("should be valid when you provide all data", async () => {
    let response = await CreateUserValidation.validate({
      fullName: "Ram",
      countryCode: 91,
    });
    verifyResultOk(() => {})(response);
  });
});
