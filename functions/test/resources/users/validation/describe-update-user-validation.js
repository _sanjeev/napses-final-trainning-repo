const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const UpdateUserValidation = require("../../../../resources/users/User/validation/update-user-validation");

describe("Update user validation", () => {
  it("FullName Should be mandatory", async () => {
    let response = await UpdateUserValidation.validate({});
    verifyResultError((error) => {
      expect(error.errorMessage).to.include("FullName should not be empty!");
    })(response);
  });

  it("FullName length is less than 2", async () => {
    let response = await UpdateUserValidation.validate({ fullName: "a" });
    verifyResultError((error) => {
      expect(error.errorMessage).to.include(
        "FullName length should always be greater than or equals to 2."
      );
    })(response);
  });

  it("countryCode Should be mandatory", async () => {
    let response = await UpdateUserValidation.validate({});
    verifyResultError((error) => {
      expect(error.errorMessage).to.include("countryCode should not be empty!");
    })(response);
  });

  it("CountryCode length will be less than 2", async () => {
    let response = await UpdateUserValidation.validate({ countryCode: 9 });
    verifyResultError((error) => {
      expect(error.errorMessage).to.include("countryCode Must be of length 2");
    })(response);
  });

  it("CountryCode length will be greater than 2", async () => {
    let response = await UpdateUserValidation.validate({ countryCode: 911 });
    verifyResultError((error) => {
      expect(error.errorMessage).to.include("countryCode Must be of length 2");
    })(response);
  });

  it("should be valid when you provide all data", async () => {
    let response = await UpdateUserValidation.validate({
      fullName: "Tom",
      countryCode: 10,
    });
    verifyResultOk(() => {})(response);
  });
});