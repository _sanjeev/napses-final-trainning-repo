const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const GetAddressQuery = require("./../../../../resources/users/Address/queries/get_address");

describe("create address query", () => {
  let user, address;
  beforeEach(async () => {
    address = await ds.createSingle(ds.address);
  });

  it("should create an address", async () => {
    const createdUserResponse = await db.execute(
      new GetAddressQuery(address.user.id)
    );
    let id = [], street = [], city = [], country = [];
    createdUserResponse.value.map((item) => {
        id.push(item.id);
        street.push(item.street);
        city.push(item.city);
        country.push(item.country);
    });
    expect(id).to.include(address.id);
    expect(street).to.include(address.street);
    expect(city).to.include(address.city);
    expect(country).to.include(address.country);

  });

  after(async () => {
    await ds.deleteAll();
  });
});
