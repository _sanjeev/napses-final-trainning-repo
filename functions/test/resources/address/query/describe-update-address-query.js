const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const Result = require("folktale/result");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const UpdateAddressQuery = require("./../../../../resources/users/Address/queries/update_address");

describe("Update address query", () => {
  let user, address;
  beforeEach(async () => {
    user = await ds.createSingle(ds.user);
    address = await ds.buildSingle(ds.address, { user });
  });

  it("should update an address", async () => {
    console.log(address);
    address.street = "Digha Beach";
    address.city = "Kolkata";
    address.country = "India";
    const updateAddressResponse = await db.execute(
      new UpdateAddressQuery(
        address.id,
        address.street,
        address.city,
        address.country
      )
    );
    verifyResultOk((updatedUser) => {
      expect(updatedUser).to.be.eql([0]);
    })(updateAddressResponse);

    const fetchAddressResppnse = await db.findOne(
      new RunQuery("select * from public.addresses where id=id", {
        id: user.id,
      })
    );
    verifyResultOk((updatedAddress) => {
      expect(updatedAddress).to.be.null;
    })(fetchAddressResppnse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
