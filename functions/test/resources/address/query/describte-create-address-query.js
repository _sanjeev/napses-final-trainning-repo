const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const CreateAddressQuery = require ('./../../../../resources/users/Address/queries/create_address');

describe("create address query", () => {
    let user,address;
  beforeEach(async () => {
      user = await ds.createSingle(ds.user);
      address = await ds.buildSingle(ds.address, {user});
  });

  it('should create an address', async() => {
      const createdUserResponse = await db.execute(new CreateAddressQuery(address.id, address.street, address.city, address.country, user.id));
      verifyResultOk(
          (createAddress) => {
              expect(address.id).eql(createAddress.id);
              expect(address.street).eql(createAddress.street);
              expect(address.country).eq(createAddress.country);
              expect(user.id).to.be.eql(address.user.id)
          }
      )(createdUserResponse);

      const fetchUserResponse = await db.findOne(new RunQuery('select * from public.addresses where id=id', {id: address.id}));
      verifyResultOk(
        (createAddress) => {
            expect(address.id).eql(createAddress.id);
            expect(address.street).eql(createAddress.street);
            expect(address.city).eq(createAddress.city);
            expect(address.country).to.be.eql(createAddress.country);
        }
    )(fetchUserResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
