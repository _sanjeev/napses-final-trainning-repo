const chai = require ('chai');
const expect = chai.expect;
const {verifyResultOk, verifyResultError} = require ('helpers/verifiers');
const CreateAddressValidation = require ('./../../../../resources/users/Address/validation/create-address-validation');

describe('Create Address Validation', () => {
    it('street should not be empty!', async() => {
        let response = await CreateAddressValidation.validate ({city: 'Delhi', country: 'India'});
        verifyResultError (
            (error) => {
                expect (error.errorMessage).to.include ("street should not be empty!");
            }
        )(response);
    });
    it('city should not be empty!', async() => {
        let response = await CreateAddressValidation.validate ({street: 'Digha Beach', country: 'India'});
        verifyResultError(
            (error) => {
                expect (error.errorMessage).to.include('city should not be empty!');
            }
        )
    });
    it ('country should not be empty!', async () => {
        let response = await CreateAddressValidation.validate ({street: 'Digha Beach', city: 'Delhi'});
        verifyResultError(
            (error) => {
                expect (error.errorMessage).to.include('country should not be empty!');
            }
        )(response);
    });
    it ('When we provide all the data', async () => {
        let response = await CreateAddressValidation.validate ({street: 'Digha Lane', city: 'Kolkata', country: 'India'});
        verifyResultOk (
            () => {

            }
        )(response);
    });
});