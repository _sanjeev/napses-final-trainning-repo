const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");

describe("Delete a particular address", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let userId = uuid.v4();
  let addressId = uuid.v4();

  beforeEach(() => {
    (req = {
      params: {
        userId: userId,
        addressId: addressId,
      },
      body: {},
    }),
      (res = {
        setHeader: sandbox.spy(),
        send: sandbox.spy(),
        status: sandbox.spy(() => {
          return res;
        }),
      });
  });

  it("should delete the particular address", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk({
          message: "Successfully delete address",
        })
      );
    const response = await TestRoutes.execute(
      "/users/:userId/addresses/:addressId",
      "Delete",
      req,
      res
    );
    expect(response).to.be.eql({
      status: true,
      message: "Successfully delete address",
      entity: {
        message: "Successfully delete address",
      },
    });
  });

  it("something went wrong cannot able to delete the address", async() => {
    sandbox
    .mock(db)
    .expects("execute")
    .returns(resolveError("some random error"));
      const response = await TestRoutes.executeWithError ('/users/:userId/addresses/:addressId', 'Delete', req, res);
      expect(response).to.be.eql(new ApiError(0, 'some random error', 'Failed to delete address'));
  })

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
