const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");

const UpdateAddressValidation = require("./../../../../resources/users/Address/validation/update-address-validation");

describe("Describe Update Address", () => {
  let sandbox = sinon.createSandbox();
  let id = uuid.v4();
  let req, res;
  beforeEach(() => {
    req = {
      params: {
        id: id,
      },
      body: {
        street: "Digha beach",
        city: "Kolkata",
        country: "India",
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  it("should update an user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk([
          {
            street: "Digha beach",
            city: "Kolkata",
            country: "India",
          },
        ])
      );
    const response = await TestRoutes.execute(
      "/users/:id/addresses/:id",
      "Put",
      req,
      res
    );
    expect(response).to.eql({
      status: true,
      message: "Successfully update the Address",
      entity: [
        {
          street: "Digha beach",
          city: "Kolkata",
          country: "India",
        },
      ],
    });
  });

  it("should not update the address when street is not passed", async () => {
    sandbox
      .mock(UpdateAddressValidation)
      .expects("validate")
      .returns(resolveValidationError(["street is mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ValidationError(0, ["street is mandatory"])
    );
  });

  it("should not update when city is not passed", async () => {
    sandbox
      .mock(UpdateAddressValidation)
      .expects("validate")
      .returns(resolveValidationError(["city is mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ValidationError(0, ["city is mandatory"])
    );
  });

  it("should not update when country is not passed", async () => {
    sandbox
      .mock(UpdateAddressValidation)
      .expects("validate")
      .returns(resolveValidationError(["country is mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ValidationError(0, ["country is mandatory"])
    );
  });

  it("something went wrong cannot able to update the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random error", "Failed to update Address")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
