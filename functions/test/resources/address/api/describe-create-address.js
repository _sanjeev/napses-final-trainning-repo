const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");

const CreateAddressValidation = require("./../../../../resources/users/Address/validation/create-address-validation");

describe("Should create an address for an user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let id = uuid.v4();
  beforeEach(() => {
    (req = {
      params: {
        id: id,
      },
      body: {
        street: "Beach",
        city: "Aasam",
        country: "India",
      },
    }),
      (res = {
        setHeader: sandbox.spy(),
        send: sandbox.spy(),
        status: sandbox.spy(() => {
          return res;
        }),
      });
  });

  it("should create an address", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk({
          street: "Beach",
          city: "Aasam",
          country: "India",
        })
      );
    const response = await TestRoutes.execute(
      "/users/:id/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql({
      status: true,
      message: "Successfully created addresses",
      entity: {
        street: "Beach",
        city: "Aasam",
        country: "India",
      },
    });
  });

  it("should not create addresses when validation failed", async () => {
    sandbox
      .mock(CreateAddressValidation)
      .expects("validate")
      .returns(resolveValidationError(["street should be mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, ["street should be mandatory"])
    );
  });

  it("should not create addresses when validation failed", async () => {
    sandbox
      .mock(CreateAddressValidation)
      .expects("validate")
      .returns(resolveValidationError(["city should be mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, ["city should be mandatory"])
    );
  });

  it("should not create addresses when validation failed", async () => {
    sandbox
      .mock(CreateAddressValidation)
      .expects("validate")
      .returns(resolveValidationError(["country should be mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, ["country should be mandatory"])
    );
  });

  it("something went wrong cannot able to fetch the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ApiError(0, "some random error", "Failed to create addresses")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
