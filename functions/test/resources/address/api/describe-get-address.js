const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");

describe("Get aadhar for a particular user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let id = uuid.v4();
  beforeEach(() => {
    (req = {
      params: {
        id: id,
      },
      body: {},
    }),
      (res = {
        setHeader: sandbox.spy(),
        send: sandbox.spy(),
        status: sandbox.spy(() => {
          return res;
        }),
      });
  });

  it("should get address for a particular user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk([
          {
            street: "Beach",
            city: "Aasam",
            country: "India",
          },
        ])
      );
    const response = await TestRoutes.execute(
      "/users/:id/addresses",
      "Get",
      req,
      res
    );
    expect(response).to.be.eql({
      status: true,
      message: "Successfully get all address for a particular users",
      entity: [
        {
          street: "Beach",
          city: "Aasam",
          country: "India",
        },
      ],
    });
  });

  it("something went wrong cannot able to get the address for a particular user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses",
      "Get",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(
        0,
        "some random error",
        "Failed to fetched address for a particular user"
      )
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
