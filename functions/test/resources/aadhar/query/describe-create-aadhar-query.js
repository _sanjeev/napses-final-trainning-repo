const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const CreateAadharQuery = require("./../../../../resources/users/Aadhar/queries/createAadhar");

describe("create aadhar query", () => {
  let user, aadhar;
  beforeEach(async () => {
    user = await ds.createSingle(ds.user);
    aadhar = await ds.buildSingle(ds.aadhar, { user });
  });

  it("should create an aadhar for an user", async () => {
    const createAadharResponse = await db.execute(
      new CreateAadharQuery(aadhar.id, aadhar.aadharNumber, user.id)
    );
    verifyResultOk((createdAadhar) => {
      expect(aadhar.id).eql(createdAadhar.id);
      expect(aadhar.aadharNumber.toString()).eql(createdAadhar.aadharNumber);
    })(createAadharResponse);

    const fetchUserResponse = await db.findOne(
      new RunQuery("select * from public.aadhar_cards where id=id", { id: aadhar.id })
    );
    verifyResultOk((createdAadhar) => {
      expect(aadhar.id).eql(createdAadhar.id);
      expect(aadhar.aadharNumber.toString()).eql(createdAadhar.aadharNumber);
    })(fetchUserResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
