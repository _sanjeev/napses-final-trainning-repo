const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateAadharValidation = require("./../../../../resources/users/Aadhar/validations/create-aadhar-validation");

describe("Create Aadhar Validation", () => {
  it("aadharNumber should not be empty!", async () => {
    let response = await CreateAadharValidation.validate({});
    verifyResultError((error) => {
      expect(error.errorMessage).to.include(
        "aadharNumber should not be empty!"
      );
    })(response);
  });
  it("aadharNumber Length Less than 12", async () => {
    let response = await CreateAadharValidation.validate({
      aadharNumber: "1234",
    });
    verifyResultError((error) => {
      expect(error.errorMessage).to.include(
        "aadharNumber Length must be of 12 digit"
      );
    })(response);
  });
  it("aadharNumber Length Greater than 12", async () => {
    let response = await CreateAadharValidation.validate({
      aadharNumber: "12345678982345",
    });
    verifyResultError((error) => {
      expect(error.errorMessage).to.include(
        "aadharNumber Length must be of 12 digit"
      );
    })(response);
  });
  it("When we provide correct result", async () => {
    let response = await CreateAadharValidation.validate({
      aadharNumber: "123456782345",
    });
    verifyResultOk(() => {})(response);
  });
});
