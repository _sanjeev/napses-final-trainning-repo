const ApiError = require("./../../../../lib/functional/api-error");
const ValidationError = require("./../../../../lib/validation-error");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { expect } = chai;
const TestRoutes = require("helpers/test-route");
chai.use(sinonChai);
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveDbResult,
  resolveOk,
  resolveError,
  validationError,
  verifyArgs,
  resolveValidationError,
} = require("helpers/resolvers");
const CreateAadharValidation = require("./../../../../resources/users/Aadhar/validations/create-aadhar-validation");

describe("Create Aadhar for a particular user", () => {
  let sandbox = sinon.createSandbox();
  let id = uuid.v4();
  let req, res;
  beforeEach(() => {
    (req = {
      params: {
        id: id,
      },
      body: {
        aadharNumber: "123456784567",
      },
    }),
      (res = {
        setHeader: sandbox.spy(),
        send: sandbox.spy(),
        status: sandbox.spy(() => {
          return res;
        }),
      });
  });

  it("should create an aadhar for particular user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(
        resolveOk({
          aadharNumber: "123456784567",
        })
      );
    const response = await TestRoutes.execute("/users/:id/aadhar", "Post", req, res);
    expect(response).to.be.eql({
      status: true,
      message: "Successfully created aadhar",
      entity: 
        {
          aadharNumber: "123456784567",
        },
      
    });
  });

  it('should create an aadhar when validation failed', async() => {
      sandbox.mock(CreateAadharValidation).expects('validate').returns(resolveValidationError(['aadharNumber is mandatory']));
      const response = await TestRoutes.executeWithError('/users/:id/aadhar', 'Post', req, res);
      expect(response).to.be.eql(new ValidationError(0, ['aadharNumber is mandatory']));
  });

  it ('something went wrong cannot able to create an aadhar', async() => {
      sandbox.mock(db).expects('execute').returns(resolveError('some random error'))
      const response = await TestRoutes.executeWithError ('/users/:id/aadhar', 'Post', req, res);
      expect(response).to.be.eql(new ApiError(0, 'some random error', 'Failed to create aadhar'));
  })

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
