const R = require ('ramda');

const findLength = R.curry ((value) => {
    return R.length(value);
})

module.exports = findLength;