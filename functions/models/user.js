"use strict";
const uuid = require("uuid");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Aadhar, { foreignKey: "aadharId" });
      this.hasMany (models.Address, {foreignKey: 'userId'});
      this.hasMany (models.Role, {foreignKey: 'userId'});
    }
  }
  User.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      fullName: DataTypes.STRING,
      countryCode: DataTypes.INTEGER,
      // aadharId: {
      //   type: DataTypes.UUID,
      //   allowNull: true
      // }
    },
    {
      sequelize,
      tableName: "users",
      modelName: "User",
    }
  );
  return User;
};
